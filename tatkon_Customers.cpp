#include <iostream>
#include <string>
#include <time.h>
#include "tatkon_Customers.h"

using namespace std;

/*===========================================================================

  Name:   Noah Tatko                Class:    CIS 161 Computer Science II
  Email:  tatkon@fontbonne.edu      Date:     2/28/2016

  Files in Project:
    tatkon_main.cpp
    tatkon_LinkedList.h
    tatkon_LinkedList.cpp
    tatkon_Customers.h
    tatkon_Customers.cpp (this file)

        (C) Copyright 2016 by Noah Tatko. All rights reserved.

  Purpose of Code:
    Have you ever been in one of those long lines where you take a number to
    wait? And then they call a number, but sometimes they never seem to call
    any names, or they call, like, 15 in a row, and nobody comes to the front?
    Well, this project seeks to digitize that same process.

    I do realize they already have a similar program in place at most DMV's,
    but it was made by the government for the civillian market, so that can't
    be good. This program seeks to do it right.

    These are the function definitions for the declarations found in
    tatkon_Customers.h, and this is actually where I use the LinkedList code.
    The Line class uses the linked list, and makes the programming of main
    much simpler. This is an example of abstraction

=============================================================================*/

// CLASS = CUSTOMERS
Customer::Customer(string n) {
	cnameds = n;
	time(&arrived);
}

Customer::Customer() {
	time(&arrived);
}

Customer::~Customer() {}

void Customer::setName(string na) {
	cnameds = na;
}

string Customer::getName() {
	return cnameds;
}

time_t Customer::getArrived() {
	return arrived;
}

int Customer::getPlace() {
	return place;
}

time_t Customer::getTime() {
	time_t now, elapsed;
	time(&now);
	elapsed = (now - arrived);
	return elapsed;
}

void Customer::updatePlace(int position) {
	place = position;
}