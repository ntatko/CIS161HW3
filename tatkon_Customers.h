#include <string>
using namespace std;

/*===========================================================================

  Name:   Noah Tatko                Class:    CIS 161 Computer Science II
  Email:  tatkon@fontbonne.edu      Date:     2/28/2016

  Files in Project:
    tatkon_main.cpp
    tatkon_LinkedList.h
    tatkon_LinkedList.cpp
    tatkon_Customers.h (this file)
    tatkon_Customers.cpp

  Purpose of Code:
    Have you ever been in one of those long lines where you take a number to
    wait? And then they call a number, but sometimes they never seem to call
    any names, or they call, like, 15 in a row, and nobody comes to the front?
    Well, this project seeks to digitize that same process.

    I do realize they already have a similar program in place at most DMV's,
    but it was made by the government for the civillian market, so that can't
    be good. This program seeks to do it right.

    This file provides the class definitions for the required class Customers,
    and the not required class of line. The line class is used to keep track
    of general attributes of the line at large, such as the total length, or
    have the ability to modify the Line at large as well, like removing a
    person, etc. The Line class is totally unnecessary, you can keep track
    of all those things just as easily within main. I just happen to like
    classes.

=============================================================================*/

class Customer {
	private:
		string cnameds;
		time_t arrived;
		int place;
		int lastServed;

	public:
		Customer(string n);
		Customer();
		~Customer();
		string getName();
		time_t getArrived();
		int getPlace();
		time_t getTime();
		void updatePlace(int position);
		void setName(string na);
};
