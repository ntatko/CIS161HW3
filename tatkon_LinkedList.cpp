#include <iostream>
#include "tatkon_LinkedList.h"
#include <string>

using namespace std;

/*===========================================================================

  Name:   Noah Tatko                Class:    CIS 161 Computer Science II
  Email:  tatkon@fontbonne.edu      Date:     2/28/2016

  Files in Project:
    tatkon_main.cpp (this file)
    tatkon_LinkedList.h
    tatkon_LinkedList.cpp
    tatkon_Customers.h
    tatkon_Customers.cpp

        (C) Copyright 2016 by Noah Tatko. All rights reserved.

  Purpose of Code:
    Have you ever been in one of those long lines where you take a number to
    wait? And then they call a number, but sometimes they never seem to call
    any names, or they call, like, 15 in a row, and nobody comes to the front?
    Well, this project seeks to digitize that same process.

    I do realize they already have a similar program in place at most DMV's,
    but it was made by the government for the civillian market, so that can't
    be good. This program seeks to do it right.

    The purpose of this code is to write the functions for the LinkedList class

=============================================================================*/

// CLASS = LinkedList

LinkedList::LinkedList() {
	head = NULL;
}

LinkedList::~LinkedList() {}

LinkedList::ListNode::ListNode(Customer v, ListNode *n) {
	value = v;
	next = n;
}

void LinkedList::add(Customer n) {
	ListNode *item = new ListNode(n , NULL);
	if (head == NULL) {
		head = item;
	} else {
		ListNode *curr = head;
		while (curr -> next != NULL) {
			curr = curr -> next;
		}
		curr -> next = item;
	}
}

void LinkedList::del(string v) {
	if (head->value.getName() == v) {
		head = head->next;
	}
	else {
		ListNode *curr = head;
		while (curr->next != NULL) {
			if (curr->next->value.getName() == v) {
				curr->next = curr->next->next;
				break;
			}
			curr = curr->next;
		}
	}
}

int LinkedList::findPlace(string v) {
	if (head == NULL) {
		return -1;
	}
	ListNode *curr = head;
	if (curr->value.getName() == v) {
		return 0;
	}
	int count = 0;
	while (curr != NULL) {
		if (curr->value.getName() == v) {
			return count;
		}
		curr = curr->next;
		count += 1;
	}
	return -1;
}

time_t LinkedList::findTime(string v) {
	if (head == NULL) {
		return -1;
	}
	ListNode *curr = head;
	if (curr->value.getName() == v) {
		return curr->value.getTime();
	}
	while (curr != NULL) {
		if (curr->value.getName() == v) {
			return curr->value.getTime();
		}
		curr = curr->next;
	}
	return -1;
}

void LinkedList::printList() {
	ListNode *curr = head;
	if (curr == NULL) {
		cout << "List is empty\n" << endl;
	}
	else {
		cout << "\tName\t\tTime waiting\n";
		int count = 1;
		while (curr != NULL) {
			cout << count << ":\t" << curr->value.getName();
			cout << "\t\t" << curr->value.getTime() << endl;
			count += 1;
			curr = curr->next;
		}
	}
}

string LinkedList::getHead() {
	return head->value.getName();
}

// CLASS = LINE
Line::Line() {}

Line::~Line() {}

void Line::addPerson(string n) {
	Customer person(n);
	link.add(person);
	link.printList();
}

void Line::firstServed() {
	link.del(link.getHead());
	link.printList();
}

void Line::deletePerson(string name) {
	link.del(name);
	link.printList();
}

int Line::findPerson(string name) {
	int placed = link.findPlace(name);
	return placed + 1;
}

time_t Line::getTime(string n) {
	time_t delta = link.findTime(n);
	return delta;
}
