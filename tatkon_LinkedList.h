#include "tatkon_Customers.h"

/*===========================================================================

  Name:   Noah Tatko                Class:    CIS 161 Computer Science II
  Email:  tatkon@fontbonne.edu      Date:     2/28/2016

  Files in Project:
    tatkon_main.cpp
    tatkon_LinkedList.h (this file)
    tatkon_LinkedList.cpp
    tatkon_Customers.h
    tatkon_Customers.cpp

    (C) Copyright 2016 by Noah Tatko. All rights reserved.

  Purpose of Code:
    Have you ever been in one of those long lines where you take a number to
    wait? And then they call a number, but sometimes they never seem to call
    any names, or they call, like, 15 in a row, and nobody comes to the front?
    Well, this project seeks to digitize that same process.

    I do realize they already have a similar program in place at most DMV's,
    but it was made by the government for the civillian market, so that can't
    be good. This program seeks to do it right.

    This particular snippet of code is the class declaration for LinkedList.

=============================================================================*/

class LinkedList {
	private:
		struct ListNode {
			Customer value;
			ListNode *next;
			ListNode(Customer v, ListNode *n);
		};
		ListNode *head;

	public:
		LinkedList();
		~LinkedList();
		void add(Customer n);
		void del(string v);
		int findPlace(string v);
		void printList();
		string getHead();
		time_t findTime(string v);
};

class Line {
	private:
		int totInLine;
		LinkedList link;
	public:
		Line();
		~Line();
		void addPerson(string name);
		void firstServed();
		void deletePerson(string name);
		int findPerson(string name);
		time_t getTime(string n);
};
