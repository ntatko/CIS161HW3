#include <iostream>
#include "tatkon_LinkedList.h"
#include <cstdbool>
#include <string>
#include <time.h>

using namespace std;

void initialOutput();
string firstWord(string in);
string secondWord(string in);
string getInputName(string in);
void displayHelpMsg();
void errorMsg(string in);
void displayHelpMsg(string parameter);
void promptForName();
void notFound();
void personMsg(string n, int pl, time_t time);
void removeMsg(string n);
void addMsg(string n);

/*===========================================================================

  Name:   Noah Tatko                Class:    CIS 161 Computer Science II
  Email:  tatkon@fontbonne.edu      Date:     2/28/2016

  Files in Project:                 URL:
    tatkon_main.cpp (this file)       ssh:  git@gitlab.com:ntatko/CIS161HW3
    tatkon_LinkedList.h               http: https://gitlab.com/ntatko/CIS161HW3/tree/master
    tatkon_LinkedList.cpp
    tatkon_Customers.h
    tatkon_Customers.cpp

        (C) Copyright 2016 by Noah Tatko. All rights reserved.

  Purpose of Code:
    Have you ever been in one of those long lines where you take a number to
    wait? And then they call a number, but sometimes they never seem to call
    any names, or they call, like, 15 in a row, and nobody comes to the front?
    Well, this project seeks to digitize that same process.

    I do realize they already have a similar program in place at most DMV's,
    but it was made by the government for the civillian market, so that can't
    be good. This program seeks to do it right.

    This program tried to act similarly to a command line, like you would find
    on a linux terminal, with a few ideas inspired by the python shell.

=============================================================================*/

/*
I realize the first thing you're going to look for in any solution is the
linked list. Well, I like to keep main really tidy, so you will find all
uses of LinkedList in the file titled tatkon_Customers.cpp, in the Line class.

I grant you that my code is very different than everyone else's. However, I
am rather OCD about my main(), so if I can keep it clean, I will abstract
everything I can. So there.

If you are looking at my homework for referrance, don't copy my code, that
would very well be the worst idea ever, as my code is very unique, and any
teacher or grader could not help but notice you had copied. That being said,
please use my code to learn from, much of computer programming is thinking up
a good many solutions to one problem. This code is one solution of many.
*/

int main() {
	Line place;
	bool quit = false;
	initialOutput();
	string input;

	while (quit == false) {
		cout << " >>  ";
		getline(cin, input);
		string answer = firstWord(input);

		if (answer == "quit" || answer == "exit" || answer == "QUIT" || answer == "EXIT" || answer == "q" || answer == "Q") {
		  // end while loop, quit
			quit = true;
		}
		else if (answer == "add" || answer == "a" || answer == "ADD" || answer == "A") {
		  // add an item to Line class, which is an implimentation of LinkedList
			string name;
			if (answer == input) {
			  // prompt for name
				promptForName();
				getline(cin, name);
			}
			else {
			  // get name from input variable
				name = getInputName(input);
			}
			place.addPerson(name);
			addMsg(name);
		}
		else if (answer == "delete" || answer == "del" || answer == "d" || answer == "DELETE" || answer == "DEL" || answer == "D") {
		  // delete a specific customer
			string name;
			if (answer == input) {
			  // prompt for name
				promptForName();
				getline(cin, name);
			}
			else {
			  // get name from input variable
				name = getInputName(input);
			}
			place.deletePerson(name);
			removeMsg(name);
		} 
		else if (answer == "help" || answer == "?" || answer == "HELP") {
		  // display a help message
			if (answer == input) {
			  // display standard help message
				displayHelpMsg();
			} else {
			  // display specific help message
				string sec = secondWord(input);
				displayHelpMsg(sec);
			}
		} 
		else if (answer == "find" || answer == "f" || answer == "FIND" || answer == "F") {
		  // find a person in the line
			string name;
			if (answer == input) {
			  // prompt for name
				promptForName();
				getline(cin, name);
			} 
			else {
			  // get name from input variable
				name = getInputName(input);
			}
			int location = place.findPerson(name);
			if (location == 0) {
			  // if the person is not found, add them to the list
				notFound();
				place.addPerson(name);
			} 
			else {
				time_t time = place.getTime(name);
				personMsg(name, location, time);
			}
		}
		else if (answer == "helped" || answer == "h" || answer == "HELPED" || answer == "H") {
		  // remove the first person from line
			place.firstServed();
		} 
		else {
		  // display an error message
			errorMsg(answer);
		}
	}
}

/*
Again, I told you I was all about keeping the main() simple...
Here are the functions I used up top to keep my line count down.
*/

void initialOutput() {
	// Displays the initial message
	cout << "\n  Hello, and welcome. To add your name to the waiting list," << endl;
	cout << "  please type add. For additional options, please type help. " << endl;
	cout << "  To quit, please type exit.\n" << endl;
}

string firstWord(string in) {
	// gets the inputted text up until the first space
	string ans;
	if (in.find(' ') != -1) {
		ans = in.substr(0,in.find(' '));
	} else {
		ans = in;
	}
	return ans;
}

void displayHelpMsg() {
  // displays a global help message
	cout << "\n  Command\t\tDescription" << endl;
	cout << "  [a]dd\t\tAdds a customer to line" << endl;
	cout << "  [d]elete\tDeletes a customer from the line" << endl;
	cout << "  [f]ind\t\tDetermines whether a customer is in line" << endl;
	cout << "  [?]help\t\tDisplays this help message" << endl;
	cout << "  [h]elped\tRemoves the customer who has been waiting the longest" << endl;
}

void displayHelpMsg(string parameter) {
  // displays a specific help message
	if (parameter == "add" || parameter == "a" || parameter == "ADD" || parameter == "A") {
		cout << "\n  [a]dd\t\tAdds a customer to line" << endl;
		cout << "  [Usage:]\tadd\t<customerName>" << endl;
		cout << "  \t\t\tadd\t[no parameters]\n" << endl;
	}
	else if (parameter == "help" || parameter == "?" || parameter == "HELP") {
		cout << "\n  [?]help\t\tDisplays a help message" << endl;
		cout << "  [Usage:]\thelp\t<functionName>" << endl;
		cout << "  \t\thelp\t[no parameters]\n" << endl;
	}
	else if (parameter == "helped" || parameter == "h" || parameter == "HELPED" || parameter == "H") {
		cout << "\n  [h]elped\tRemoved the first person from the list" << endl;
		cout << "  [Usage:]\thelped\t[no parameters]\n" << endl;
	}
	else if (parameter == "find" || parameter == "f" || parameter == "FIND" || parameter == "F") {
		cout << "\n  [f]ind\t\tFind the position of a person in the list" << endl;
		cout << "  [Usage:]\tfind\t<customerName>" << endl;
		cout << "  \t\tfind\t[no parameters]\n" << endl;
	}
	else if (parameter == "delete" || parameter == "del" || parameter == "d" || parameter == "D" || parameter == "DEL" || parameter == "DELETE") {
		cout << "\n  [d]elete\tDeletes a person from within the list" << endl;
		cout << "  [Usage:]\tdelete\t<customerName>" << endl;
		cout << "  \t\tdelete\t[no parameters]\n" << endl;
	}
	else {
		displayHelpMsg(parameter);
	}
}

void errorMsg(string in) {
	// displays the invalid input error message
	cout << "\n'" << in << "' is not recognized as an internal or" << endl;
	cout << "  external command. \n" << endl;
}

string secondWord(string in) {
	// gets the second word in an input string
	string ans;
	in.erase(0, in.find(' ') + 1);
	if (in.find(' ') != -1) {
		ans = in.substr(0,in.find(' '));
	} else {
		ans = in;
	}
	return ans;
}

string getInputName(string in) {
	// gets the name entered on the command line
	string ans;
	in.erase(0, in.find(' ') + 1);
	if (in.find(' ') != -1) {
		ans = in.substr(0,in.find(' '));
	}
	else {
		ans = in;
	}
	in.erase(0, in.find(' ') + 1);
	if (in.find(' ') != -1) {
		ans += " " + in.substr(0,in.find(' '));
	}
	return ans;
}

void promptForName() {
  // prompts the user for a name
	cout << "  Name?\t";
}

void notFound() {
	// displays the not found error message
	cout << "  Sorry, the person you're looking for could not be found." << endl;
	cout << "  We have added you to the line, and we will be with you" << endl;
	cout << "  as soon as we can.\n" << endl;
}

void personMsg(string n, int pl, time_t time) {
  // show information about a person's time in line.
	if (pl == 0) {
		cout << "\n  " << n << " is not in the list.\n" << endl;
	}
	else {
		cout << "\n  " << n << " is the " << pl;
		if (pl % 10 == 1) {
			if (pl == 11) {
				cout << "th ";
			}
			else {
				cout << "st ";
			}
		}
		else if (pl % 10 == 2) {
			if (pl == 12) {
				cout << "th ";
			}
			else {
				cout << "nd ";
			}
		}
		else if (pl % 10 == 3) {
			if (pl == 13) {
				cout << "th ";
			}
			else {
				cout << "rd ";
			}
		}
		else {
			cout << "th ";
		}
		cout << "person in line.\n";

		cout << firstWord(n) << " has been waiting for ";
		cout << time << " seconds.\n" << endl;
	}
}

void addMsg(string n) {
	cout << "\n  " << n << " has been added to the line.\n" << endl;
}

void removeMsg(string n) {
	cout << "\n  " << n << " has been removed from the line.\n" << endl;
}